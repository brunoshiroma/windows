# Librewolf Windows

## Instalation/Creation
 * clone this repository
 * execute the package.ps1 on powershell  

 This is create a folder *librewolf* on current dir, with subfolder core (its content of the firefox.exe), with Firefox configured with scripts/settings of librewolf.  
 From that point you can install using the setup.exe or simple execute the ***firefox.exe*** on the core folder.

## Cloning the repo
```powershell
git clone --recursive https://gitlab.com/librewolf-community/browser/windows.git
```

### Opensource Licences/Uses
This scripts use and embed [7zip](https://www.7-zip.org/)
